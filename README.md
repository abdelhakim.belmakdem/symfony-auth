# SymfonyAuth
 
### Dependencies
 - symfony 5
 - php 7.4
 - postgres 12
 - composer 1.10
 - node 16.20
 - npm 8.19.4

## Install dependencies

Change your .env file into .env.[env] and enter the credentials for postgres and run the following:

```
    
    # Install composer dependencies
    composer install
    
    # Install npm dependencies
    
    npm install
    
    # Compile assets
    
    npm watch

    # Run symfony
    symfony serve
``` 

Open your browser on : http://localhost:7080/  

### Init database

If you need data to work you need to run :

```
    # Create database
    php ./bin/console d:d:c

    # Update scheme
    php ./bin/console d:s:u --force

```

### Tools

Clear cache :
`php ./bin/console cache:clear`
